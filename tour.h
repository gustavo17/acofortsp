#pragma once

#include "mapa.h"
#include "nodo.h"
#include <optional>
#include <random>
#include <vector>

#define ENTRE(menor, mayor, entre) \
    ((menor->posicion <= entre->posicion && entre->posicion <= mayor->posicion) || \
     ((mayor->posicion < menor->posicion) && ((menor->posicion <= entre->posicion) || (entre->posicion <= mayor->posicion))))


class Tour {
    public:
      Tour(std::vector<int> ruta);
      Tour(const Tour &sol);
      Tour(Tour &&sol);
      Tour &operator=(Tour &&sol);


      void mostrar() const;
      int evaluar(const Mapa &m) ;
      int twoOpt(std::mt19937 &engine, const Mapa &m, const std::optional<Tour> & mejor);
      void mover(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3);
      bool isConexa() const;
      bool operator<(const Tour &otro) const;
      bool operator>(const Tour &otro) const;
      void busquedaLocal(int MAX, std::mt19937 &engine, const Mapa &mapa,
                         const std::optional<Tour> mejor = std::nullopt);

      int costo;
      int largo;
  
      std::vector<Nodo> ady;
};