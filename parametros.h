#pragma once
#define FORMULA_CALCULO_FEROMONA(parametro) parametro/(tour.costo-86700.0)

const int ITERACIONES=100;
const int NHILOS=6;
const int NHORMIGAS=40;
const double RO=0.02;
const double TAU_MIN=0.001;
const double TAU_MAX=4;
const int NSINMEJORA=300;
const double ALFA=0.57;  //distancia
const double BETA=1.0;  //historia
const int LISTA_CANDIDATOS=25;
