#include <iostream>
#include <vector>
#include <optional>
#include <thread>
#include "colonia.h"
#include "parametros.h"
#include "tour.h"
#include "ant.h"

void Colonia::moverHormigas() {
    std::vector <std::optional<Tour>> soluciones (NHORMIGAS);
    std::vector <std::thread> hilos;
    std::mutex mtx;
    int ind=0;

    for (int i=0;i<NHILOS;i++){
        hilos.emplace_back([&mtx,&soluciones,&ind,this](){
            std::random_device seeder;
            const auto seed {seeder.entropy() ? seeder() : time(nullptr)};
            std::mt19937 engine { static_cast<std::mt19937::result_type>(seed)};
            while (true){
                mtx.lock();
                int index=ind++;
                mtx.unlock();
                
                if (index>=NHORMIGAS) break; //te saca del while

                Ant a {tau, m,engine};
                Tour t=a.buildTour();
                t.busquedaLocal(1064, engine, m);
                soluciones[index]=std::move(t);

            }
        });
    }
    for (auto &h:hilos){
        h.join();
    }

    std::sort(soluciones.begin(),soluciones.end());

    //calculo de estadisticas
    double suma=0.0;
    for (auto &sol:soluciones){
        suma+=static_cast<double>(sol->costo);
    }
    media=suma/soluciones.size();
    suma=0.0;
    for (auto &sol:soluciones){
        suma+=static_cast<double>((sol->costo-media)*(sol->costo-media));
    }
    desv=std::sqrt(suma/soluciones.size());
    peor=soluciones[soluciones.size()-1]->costo;
    //fin


    Tour &mejorIteracion=soluciones[0].value();
    
    if (!mejor.has_value() || mejor->costo>mejorIteracion.costo){
        mejor=std::move(mejorIteracion);
    }

    //EVAPORACION ADICIONAL DE LA PEOR HORMIGA
    evaporarAdicional(soluciones[soluciones.size()-1].value());
    //updateFeromonas(soluciones[0].value(), 1.0);

}

Colonia::Colonia(const Mapa &_m):m{_m} {
    int n=m.data.size();

    tau.resize(n);
    for (int i=0;i<n;i++){
        tau[i].resize(n,TAU_MIN); //inicializa valores a valor minimo
    }
}
void Colonia::updateFeromonas(const Tour &tour, double factor) {
    for (auto &n: tour.ady){
        int origen=n.id;
        int destino=n.siguiente->id;
        double valor=tau[origen][destino]+FORMULA_CALCULO_FEROMONA(factor);
        if (valor>TAU_MAX){
            valor=TAU_MAX;
        }
        tau[origen][destino]=valor;
        tau[destino][origen]=valor;
    }
}

void Colonia::evaporar() {
    for (auto &vectorFeromonas : tau){
        for (auto &valor: vectorFeromonas){
            valor=(1-RO) * valor;
            if (valor<TAU_MIN) valor=TAU_MIN;
        }
    }
}

void Colonia::ciclo() {
    moverHormigas();
    evaporar();
    updateFeromonas(mejor.value(),10.0);
}

void Colonia::evaporarAdicional(const Tour &tour) {
    for (auto &n:tour.ady){
        int origen=n.id;
        int destino=n.siguiente->id;
        if (mejor->ady[origen].siguiente->id!=destino && mejor->ady[origen].anterior->id!=destino){
            double valor=(1-RO) * tau[origen][destino];
            if (valor<TAU_MIN) valor=TAU_MIN;
            tau[origen][destino]=valor;
            tau[destino][origen]=valor;
        }
    }
}
void Colonia::mostrarFeromonas() {
    int i=0;
    for (auto &valor:tau[0]){
        if (i++>10) break;
        std::cout << valor << " " ;
    }
    std::cout<<std::endl;
}
