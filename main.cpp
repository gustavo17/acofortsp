#include <assert.h>
#include <iostream>
#include "colonia.h"
#include "mapa.h"
#include "parametros.h"


int main() {
    Mapa m {"../att532.dat"};
    m.computarProbabilidades();
    m.computarCandidatos(m.data.size()-1); //computa todas las ciudades en orden (menos el mismo)

    auto inicio = std::chrono::high_resolution_clock::now();
    int optimos=0;
    int suma=0;
    std::optional<Tour> mejor;
    for (int j=0;j<ITERACIONES;j++){

        Colonia colonia {m};
        int nSinMejora=0;
        int costoMejor=10'000'000;
        int i=0;
        while (nSinMejora<NSINMEJORA){
            colonia.ciclo();
            //colonia.mostrarFeromonas();
            if (colonia.mejor->costo<costoMejor) {
                costoMejor=colonia.mejor->costo;
                nSinMejora=0;
                std::cout<<"#: "<<j<<", ITER:  "<<i<<", MEJOR: "<<colonia.mejor->costo<<", PEOR:"<<colonia.peor;
                std::cout<<", MEDIA: "<<colonia.media<<", DESV:"<<colonia.desv<<std::endl;

            } else {
                nSinMejora++;
            }
            i++;
        }
        if (colonia.mejor->costo==86756)optimos++;
        suma+=colonia.mejor->costo;
        if (!mejor.has_value() || mejor->costo>colonia.mejor->costo){
            mejor=std::move(colonia.mejor);
        }
    }

    auto fin = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> duracion = fin - inicio;
    mejor->mostrar();
    std::cout<<"duracion Total: "<<duracion.count()<<std::endl;
    std::cout<<"optimos: "<<optimos<<std::endl;
    std::cout<<"promedio: "<<static_cast<double>(suma)/ITERACIONES<<std::endl;
    std::cout<<"mejor: "<<mejor->costo<<std::endl;
    return 0;
}


