#include "tour.h"
#include <optional>
#include <random>
#include <vector>
#include <iostream>
#include <cassert>


void Tour::mostrar() const {
    std::cout<<"0,";
    const Nodo * inicio=&ady[0];
    const Nodo * actual=inicio->siguiente;
    while (actual!=inicio){
        std::cout<<actual->id<<",";
        actual=actual->siguiente;
    }
    std::cout<<"0"<<std::endl;
}

int Tour::evaluar(const Mapa &m)  {
    Nodo *inicio=&ady[0];
    Nodo *ptr=inicio->siguiente;
    int suma=0;
    while(ptr!=inicio) {
        suma+=m.data[ptr->anterior->id][ptr->id];
        ptr=ptr->siguiente;
    }
    suma+=m.data[ptr->anterior->id][ptr->id];
    return suma;
}

//esta funcion, dado un tour, selecciona un movimiento con ganancia positiva, si es que existe alguno
//se basa en el algoritmo LK
//se utiliza el criterio de Ganancia positiva para ser seleccionado
//analiza 2-opt primero, si no 3-opt, si no devuelve ganancia cero
//elige una ciudad inicial al azar y una arista que no se encuentre en el mejor de la poblacion
//parametros: engine de numeros aleatorios
//mapa: lista de distancias entre dos ciudades
//mejor: mejor tour de la poblacion hasta este momento
//RETORMNO: valor de la ganancia encontrado>0 o 0 si no se encuentra
int Tour::twoOpt(std::mt19937 &engine, const Mapa &m, const std::optional<Tour> & mejor) {
    std::uniform_int_distribution<int> aleatorio(0,largo-1);
    Nodo * inicio=&ady[aleatorio(engine)];
    Nodo *t0=inicio;
    Nodo *t1;
    
    while (true){
        t1=t0->siguiente;
        if (!mejor.has_value()) break;
        if (mejor->ady[t0->id].siguiente->id!=t1->id && mejor->ady[t0->id].anterior->id!=t1->id) break;
        t0=t0->siguiente;
        if (t0==inicio) return 0; // sale al dar la vuelta completa (llegar a inicio de nuevo)
    }
    

    for (int nodo: m.candidatosDeNodo[t1->id]){
        Nodo *t2=&ady[nodo];
        if (t1->siguiente==t2 || t0==t2) continue; //no elige los que ya estan en el tour
        int g0=m.data[t0->id][t1->id]-m.data[t1->id][t2->id];
        if (g0<=0) continue;
        Nodo *t3=t2->anterior;
        int ganancia=g0+m.data[t2->id][t3->id]-m.data[t3->id][t0->id];
        if (ganancia>0){
            int t3t1=t3->posicion-t1->posicion; if (t3t1<0) t3t1+=ady.size();
            int t0t2=t0->posicion-t2->posicion; if (t0t2<0) t0t2+=ady.size();
            if (t3t1<t0t2)
                mover(t0, t1, t2, t3);
            else 
                mover(t3, t2, t1, t0);
            return ganancia;
        } else {
            //3-opt
            for (auto ct4:m.candidatosDeNodo[t3->id]){
                Nodo *t4=&ady[ct4];
                int G1=g0+m.data[t2->id][t3->id]-m.data[t3->id][t4->id];
                if (G1<=0 || t4==t3->siguiente || t4==t3->anterior)continue;
                bool post4=ENTRE(t1,t3,t4);
                Nodo* t5=post4?t4->siguiente:t4->anterior;
                int g2=G1+m.data[t4->id][t5->id]-m.data[t5->id][t0->id];
                if (g2>0){
                    mover(t0,t1, t2, t3);
                    mover(t0,t3, t4, t5);
                    return g2;
                }
            }
        }
    }
    return 0; 
}

void Tour::mover(Nodo *t0, Nodo *t1, Nodo *t2, Nodo *t3) {
    int pos=t3->posicion;
    Nodo *ptr=t1;
    while (ptr!=t2){
        ptr->posicion=pos--;
        if (pos<0)pos=largo-1;
        std::swap(ptr->siguiente, ptr->anterior);
        ptr=ptr->anterior;
    }
    t0->siguiente=t3; t3->anterior=t0;
    t1->siguiente=t2; t2->anterior=t1;
}

bool Tour::isConexa() const {
    //es mas eficiente que bool (por empaquetado)
    const Nodo *inicio=&ady[0];
    Nodo * actual=inicio->siguiente;
    int cont=1;
    int pos=inicio->posicion;
    pos++;if (pos==ady.size())pos=0;
    while (actual!=inicio){
        if (pos!=actual->posicion) {
            std::cout<<"esperada : "<<pos<<", encontrada :"<<actual->posicion<< ", en nodo "<<actual->id<<std::endl;
            return false;
        }
        pos++;if (pos==ady.size())pos=0;
        cont++;
        if (actual->siguiente->anterior!=actual){
            std::cout<<"anterior mal configurado en nodo: "<<actual->siguiente->id<<std::endl;
            return false;
        }
        actual=actual->siguiente;
        if (cont>ady.size()) return false;
    }
    return cont==ady.size();
}

Tour::Tour(std::vector<int> ruta):largo(ruta.size()-1),ady(ruta.size()-1) {
    largo=ady.size();
    auto anterior=ruta.begin();
    int posicion=0;
    auto inicio=ruta.begin()+1;
    for (auto it=inicio;it!=ruta.end();it++){
        posicion++;if (posicion==largo)posicion=0;
        Nodo *ptr=&ady[*it];
        ptr->posicion=posicion;
        ptr->id=*it;
        ptr->anterior=&ady[*anterior];
        if (it==ruta.end()-1){
            ptr->siguiente=&ady[*inicio];
        } else {
            ptr->siguiente=&ady[*(it+1)];
        }
        anterior=it;
    }
}

Tour::Tour(const Tour &sol):ady(sol.ady.size()),largo{sol.largo}{
   //std::cout<<"copiando tour"<<std::endl;
    for (int i=0;i<ady.size();i++){
        ady[i].id=sol.ady[i].id;
        ady[i].posicion=sol.ady[i].posicion;
        ady[i].siguiente=&ady[sol.ady[i].siguiente->id];
        ady[i].anterior=&ady[sol.ady[i].anterior->id];
    }
    costo=sol.costo;
}

Tour::Tour(Tour &&sol):ady{std::move(sol.ady)},costo{sol.costo},largo{sol.largo} {
    //std::cout<<"moviendo tour"<<std::endl;
}

Tour &Tour::operator=(Tour &&sol) {
    //std::cout<<"asignacion de movimiento tour"<<std::endl;
    ady=std::move(sol.ady);
    costo=sol.costo;
    largo=sol.largo;
    return *this;
}

bool Tour::operator<(const Tour &otro) const { return costo < otro.costo; }
bool Tour::operator>(const Tour &otro) const {return costo>otro.costo;}
void Tour::busquedaLocal(int MAX, std::mt19937 &engine, const Mapa &mapa,
                         const std::optional<Tour> mejor) {

    //busqueda local
    int nSinMejora=0;
    while (nSinMejora<MAX){
        int ganancia=twoOpt(engine, mapa,mejor);
        if (ganancia>0){
            nSinMejora=0;
            costo-=ganancia;
        } else {
            nSinMejora++;
        }
    }
}
