#pragma once

#include "mapa.h"
#include "tour.h"
#include "ant.h"
#include <optional>

extern const int NHORMIGAS;
extern const int NHILOS;
extern const double RO;
extern const double TAU_MIN;
extern const double TAU_MAX;


class Colonia {
    public:
      Colonia(const Mapa &_m);
      void moverHormigas();
      void updateFeromonas(const Tour &tour,double factor);
      void evaporarAdicional(const Tour &tour);
      void evaporar();
      void ciclo();
      void mostrarFeromonas();
      std::optional<Tour> mejor;
      double media,desv,peor;

    private:
        const Mapa &m; //aqui dentro esta nu (heuristicas)
        std::vector<std::vector<double>> tau; //feromonas
};