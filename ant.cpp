#include "ant.h"
#include <queue>
#include <random>
#include <iostream>

Ant::Ant(const std::vector<std::vector<double>> &tau,const Mapa &m, std::mt19937 &engine): m_m{m},m_engine{engine},m_visitadas(m.data.size(),false),m_tau{tau} {
    m_largo=m.data.size();
    std::uniform_int_distribution<int> dist(0,m_largo-1);

    m_id_ciudad_actual=dist(engine);
    m_visitadas[m_id_ciudad_actual]=true;
    rutaSeguida.push_back(m_id_ciudad_actual);
}

int Ant::mover() {
    std::vector<int> noVisitadas;
    std::vector<double> pesos;

    // pone en no visitadas las 25 mejores
    for (int cand:m_m.candidatosDeNodo[m_id_ciudad_actual]) {
        //calcula los pesos
        if (!m_visitadas[cand]){
            noVisitadas.push_back(cand);
            double peso=std::pow(m_m.probabilidadInicial[m_id_ciudad_actual][cand],ALFA)*std::pow(m_tau[m_id_ciudad_actual][cand],BETA);
            pesos.push_back(peso);
            if (noVisitadas.size()==LISTA_CANDIDATOS) break;
        }
    }
    if (noVisitadas.size()==0) return 0;

    std::discrete_distribution<> dist(pesos.begin(),pesos.end());
    int nuevaCiudad=noVisitadas[dist(m_engine)];
    m_id_ciudad_actual=nuevaCiudad;
    rutaSeguida.push_back(nuevaCiudad);
    m_visitadas[nuevaCiudad]=true;
    return 1;
}

void Ant::mostrarRuta() {
    for (auto ciudad : rutaSeguida){
        std::cout<<ciudad<<" ";
    }
    std::cout<<std::endl;
}

Tour Ant::buildTour() {
    while (mover()>0);

    std::vector<int> ruta=rutaSeguida; //copia
    ruta.push_back(rutaSeguida[0]);

    Tour t {ruta};
    t.costo=t.evaluar(m_m);
    return t;
}