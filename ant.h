#pragma once

#include "mapa.h"
#include "parametros.h"
#include "tour.h"
#include <random>
#include <vector>

extern const double ALFA; // mas cercano a cero mas grande pq prob es menor que 1
extern const double BETA;
extern const int LISTA_CANDIDATOS;

class Ant {
    public:
      Ant(const std::vector<std::vector<double>> &tau,const Mapa &m,std::mt19937 &engine);
      int mover();
      void mostrarRuta();
      std::vector<int> rutaSeguida;
      Tour buildTour();

    private:
        std::vector<bool> m_visitadas;
        int m_id_ciudad_actual;
        const Mapa &m_m;
        std::mt19937 &m_engine;
        int m_largo;
        const std::vector<std::vector<double>> &m_tau;
};